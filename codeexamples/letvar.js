


function varvslet() {
    console.log("i before declaration: " + i); // i is undefined due to hoisting
    // console.log(j); // ReferenceError: j is not defined
  
    for( var i = 0; i < 3; i++ ) {
      console.log("i in for loop: " + i); // 0, 1, 2
    };
  
    console.log("i after loop: " + i); // 3 still accessible outside block
    // console.log(j); // ReferenceError: j is not defined
  
    for( let j = 0; j < 3; j++ ) {
      console.log("let in for loop: " + j);
    };
  
    // console.log(j); // ReferenceError: j is not defined
  }

  varvslet()